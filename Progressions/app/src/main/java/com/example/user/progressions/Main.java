package com.example.user.progressions;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    Switch sw;
    Button next;
    EditText et1,et2;
    double a,x;
    boolean type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sw=(Switch)findViewById(R.id.switch1);
        next=(Button)findViewById(R.id.button);
        et1=(EditText)findViewById(R.id.editText);
        et2=(EditText)findViewById(R.id.editText2);
    }

    public void next(View view) {
        String st=et1.getText().toString();
        if(st.equals("")|st.equals(".")|st.equals("-")|st.equals("-.")){
            Toast.makeText(this, "You didn't enter the first term", Toast.LENGTH_SHORT).show();
        }
        else{
            a=Double.parseDouble(st);
            st=et2.getText().toString();
            if(st.equals("")|st.equals(".")|st.equals("-")|st.equals("-.")){
                Toast.makeText(this, "You didn't enter the difference", Toast.LENGTH_SHORT).show();
            }
            else{
                x=Double.parseDouble(st);
                if(sw.isChecked()){
                    type=true;
                }
                else{
                    type=false;
                }
                Intent next=new Intent(this,Output.class);
                next.putExtra("type",type);
                next.putExtra("first term",a);
                next.putExtra("difference",x);
                startActivity(next);
            }

        }

    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent cr=new Intent(this,Credits.class);
        startActivity(cr);
        return true;
    }
}
