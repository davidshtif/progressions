package com.example.user.progressions;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class Output extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    boolean type;
    double a,x,Sn2;
    String[] progression;
    Spinner sp;
    ImageView difference;
    TextView x1,dORq,n,Sn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output);


        Intent get=getIntent();
        type=get.getBooleanExtra("type",false);
        a=get.getDoubleExtra("first term",0);
        x=get.getDoubleExtra("difference",0);

        x1=(TextView)findViewById(R.id.x1);
        dORq=(TextView)findViewById(R.id.dORq);
        n=(TextView)findViewById(R.id.n);
        Sn=(TextView)findViewById(R.id.Sn);
        sp=(Spinner)findViewById(R.id.spinner);
        difference=(ImageView)findViewById(R.id.difference) ;

        if(type){
            difference.setImageResource(R.drawable.q);
        }
        else{
            difference.setImageResource(R.drawable.d);
        }
        sp.setOnItemSelectedListener(this);
        progression=new String[20];
        progression=fillArray(progression,type,a,x);
        ArrayAdapter<String>adp=new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item, progression);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adp);
    }

    public String[] fillArray(String[]st,boolean type,double a,double x){
        if(type){
            for(int i=1;i<=20;i++){
                st[i-1]=""+a*Math.pow(x,i-1);
            }
        }
        else{
            for(int i=1;i<=20;i++){
                st[i-1]=""+(a+(i-1)*x);
            }
        }
        return st;
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent cr=new Intent(this,Credits.class);
        startActivity(cr);
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        x1.setText("="+a);
        dORq.setText("="+x);
        String st=""+(position+1);
        n.setText("="+st);
        if(type){
            Sn2=(a*(Math.pow(x,(position+1))-1))/(x-1);
        }
        else{
            Sn2=((position+1)*(2*a+position*x))/2;
        }
        Sn.setText("="+Sn2);
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void back1(View view) {
        finish();
    }
}
